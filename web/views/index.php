<?php
/**
 * index.php.
 *
 * TODO: Documentation required!
 */

?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Bob's Time Tracker</title>
</head>
<body>
	<header>
		<h2>Time Tracker for Bob</h2>
	</header>
	<section>
		<ul>
			<li>
				<h3>Project X</h3>
				<ul>
					<li>
						<article>
							<h4>Task A</h4>
							<p>Total progress time</p>
							<input id="txtTaskProgressTotal" type="text" value="00:00:00">
							<p>Register progress for Task A</p>
							<dl>
								<dt>Date</dt>
								<dd>
									<input id="txtProgressDate" type="text" value="12/12/18">
								</dd>
								<dt>Start</dt>
								<dd>
									<input id="txtProgressStart" type="text" value="12:30">
								</dd>
								<dt>End</dt>
								<dd>
									<input id="txtProgressEnd" type="text" value="13.45">
								</dd>
							</dl>
							<dl>
								<dt>Register time</dt>
								<dd>
									<input type="text" value="01:15:00">
								</dd>
								<dd><input id="btnRegisterProgress" type="submit" value="register progress"></dd>
							</dl>
						</article>
					</li>
				</ul>
			</li>
		</ul>
	</section>
	<script type="text/javascript" src="/js/main.js"></script>
</body>
</html>