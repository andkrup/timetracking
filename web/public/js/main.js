window.onload = function(){
	let taskTotalLmnt = document.getElementById('txtTaskProgressTotal');
	let progressDateLmnt = document.getElementById('txtProgressDate');
	let progressStartLmnt = document.getElementById('txtProgressStart');
	let progressEndLmnt = document.getElementById('txtProgressEnd');
	let btnLmnt = document.getElementById('btnRegisterProgress');

	progressStartLmnt.change = function(event){
		console.log('sum up start and end time and update time-to-register');
	};
	progressEndLmnt.change = function(event){
		console.log('sum up start and end time and update time-to-register');
	};


	btnLmnt.onclick = function(event){
		console.log('Form a json payload with date, start and end time and send it to the endpoint /task/1/progress.');
		console.log('On response, update the total progress time element');
	};
};
