<?php
require_once __DIR__ . '/../../vendor/autoload.php';


//if (isset($_SERVER['APP_ENV']) && stripos($_SERVER['APP_ENV'], 'prod') !== 0) {
//    error_reporting(E_ALL);
//    $debug = new \Phalcon\Debug();
//    $debug->listen();
//}

use Phalcon\Di\FactoryDefault;
use Phalcon\Events\Event;
use Phalcon\Mvc\Micro;
use SuperVillainHQ\TimeTracker\Dependency\Auth;
use SuperVillainHQ\TimeTracker\TimeTrackerWebApp;

date_default_timezone_set('UTC');

$config = include __DIR__ . "/../../resource/config.php";

$di = new FactoryDefault();

$di->set('config', $config);


/**
 * Custom authentication component, currently useless since we're not authenticating anything
 */
$di->set('auth', function () {
    return new Auth();
});



$application = new Micro($di);

TimeTrackerWebApp::appendRoutes($application);

// default start for SPA bootstrap
$application->get(
    "/",
    function () {
    	header("Content-Type: text/html");
		include "../views/index.php";
    }
);

$application->handle();
