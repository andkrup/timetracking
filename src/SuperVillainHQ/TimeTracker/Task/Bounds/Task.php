<?php

namespace SuperVillainHQ\TimeTracker\Task\Bounds {

	use Phalcon\Di;
	use SuperVillainHQ\Core\Boundary\Boundary;
	use SuperVillainHQ\TimeTracker\Api\V01M01\TaskHelper;
	use SuperVillainHQ\TimeTracker\Task\Control\TaskService;

	/**
	 * Class Progress
	 * @package SuperVillainHQ\TimeTracker\Task\Bounds
	 */
	class Task extends Boundary	{
		function create(int $projectId){}
		function get(int $id){}

		function registerProgress(int $id){
			// TODO: validate and authenticate the request
			$result = ['response' => false];

			if($task = TaskService::findById($id)){
				// Fetch payload - Should be formalised into a custom Request object that can handle our custom json protocol
				$request = Di::getDefault()->get('request');
				$json = $request->getJsonRawBody();
				// wrapping the task servicing in a helper that is format-specific so we can support changes in format
				$taskHelper = new TaskHelper($task);
				$taskHelper->appendProgressFromPayload($task, $json);

				$result = ['response' => true];
			}
			// TODO: more advanced try/catching and response handling/error signalling could be relevant
			$response = Di::getDefault()->get('response');
			$response->setContentType('application/json');
			$response->setContent(json_encode($result));
			return $response;
		}

		function revokeProgress(int $id){}

		function alter(int $id, array $parameters){}
		function alterPartial(int $id, array $parameters){}
		function search(array $filter){}
	}
}


