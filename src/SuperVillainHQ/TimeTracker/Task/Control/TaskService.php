<?php

namespace SuperVillainHQ\TimeTracker\Task\Control {

	use SuperVillainHQ\TimeTracker\Progress\Model\Progress;
	use SuperVillainHQ\TimeTracker\Task\Model\Task;

	/**
	 * Class TaskService
	 * @package SuperVillainHQ\TimeTracker\Task\Control
	 */
	class TaskService{
		/**
		 * @var Task
		 */
		private $task;

		function __construct(Task $task){
			$this->task = $task;
		}

		public static function findById(int $id):array{
			$task = new Task(['id' => $id]);
			return [$task];
		}

		// php-style method overloading
		function addProgress(Progress $progress){
			$this->task->addProgressItem($progress);
		}

		function addProgressTime(\DateTime $date, int $hours, int $minutes = 0, int $seconds = 0){
			// NB! Assume timezones from local clients. May have consequences if Tasks only allow registering progress
			// within a limited time-window
			$startDate = new \DateTime(); // TODO: use parameters!
			$endDate = new \DateTime(); // TODO: use parameters!
			$this->addProgressDateInterval($startDate, $endDate);
		}

		function addProgressDateInterval(\DateTime $start, \DateTime $end){
			// NB! Assume timezones from local clients. May have consequences if Tasks only allow registering progress
			// within a limited time-window
			$progress = new Progress();
			$progress->setStartDate($start);
			$progress->setEndDate($end);
			$this->addProgress($progress);
		}
	}
}


