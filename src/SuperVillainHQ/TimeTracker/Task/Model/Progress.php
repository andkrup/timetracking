<?php

namespace SuperVillainHQ\TimeTracker\Progress\Model {

	/**
	 * Class Progress.
	 *
	 * @package SuperVillainHQ\TimeTracker\Progress\Model
	 */
	class Progress{
		private $startDate;
		private $endDate;
		private $description;
		private $notes;
		private $tags;
		private $user;

		/**
		 * @param mixed $startDate
		 */
		public function setStartDate($startDate){
			$this->startDate = $startDate;
		}

		/**
		 * @return mixed
		 */
		public function getStartDate(){
			return $this->startDate;
		}

		/**
		 * @param mixed $endDate
		 */
		public function setEndDate($endDate){
			$this->endDate = $endDate;
		}

		/**
		 * @return mixed
		 */
		public function getEndDate(){
			return $this->endDate;
		}

		function totalTimeSeconds():int{}
	}
}


