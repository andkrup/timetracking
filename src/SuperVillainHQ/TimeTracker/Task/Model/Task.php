<?php

namespace SuperVillainHQ\TimeTracker\Task\Model {

	use SuperVillainHQ\Core\MockModel;
	use SuperVillainHQ\TimeTracker\Progress\Model\Progress;

	/**
	 * Class Task
	 * @package SuperVillainHQ\TimeTracker\Task\Model
	 */
	class Task extends MockModel{

		private $id;

		private $progressItems;

		function __construct(\stdClass $data = null){
			$this->resetProgressItems();

			if(!is_null($data)){
				self::inflate($this, $data);
			}
		}

		function resetProgressItems(array $default = []){
			$this->progressItems = $default;
		}

		function addProgressItem(Progress $progress){}
		function hasProgressItem(Progress $progress):bool{}
		function removeProgressItem(Progress $progress){}
		function removeProgressItemAt(int $index){}
		function progressItems():array{
			return $this->progressItems;
		}
	}
}


