<?php

namespace SuperVillainHQ\TimeTracker {

	use Phalcon\Mvc\Micro;
	use Phalcon\Mvc\Micro\Collection;

	/**
	 * Class TimeTrackerWebApp
	 * @package SuperVillainHQ\TimeTracker
	 */
	class TimeTrackerWebApp{
		public static function appendRoutes(Micro $application){
			// TODO: externalise routes instead of hardcode here
			// Client bounds
			$clientCollection = new Collection();

			$clientCollection->setPrefix('/client')
				->setHandler('\SuperVillainHQ\TimeTracker\Client\Bounds\Client')
				->setLazy(true);

			$clientCollection->get('/', 'search');
			$clientCollection->get('/{id}', 'get');
			$application->mount($clientCollection);

			// Task bounds
			$taskCollection = new Collection();

			$taskCollection->setPrefix('/task')
				->setHandler('\SuperVillainHQ\TimeTracker\Task\Bounds\Task')
				->setLazy(true);

			$taskCollection->get('/', 'search');
			$taskCollection->get('/{id}', 'get');
			$taskCollection->post('/', 'create');
			$taskCollection->post('/{id}/progress', 'registerProgress');
			$taskCollection->delete('/{id}/progress', 'revokeProgress');
			$application->mount($taskCollection);
		}
	}
}


