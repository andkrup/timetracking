<?php

namespace SuperVillainHQ\TimeTracker\Client\Model {

	use SuperVillainHQ\Core\MockModel;

	/**
	 * Class Client
	 * @package SuperVillainHQ\TimeTracker\Client\Model
	 */
	class Client extends MockModel{
		public $id;
		public $name;
		private $projects;
		/*
		 * AddressBook $contacts
		 */
		private $contacts;

		function projects():array{
			return $this->projects;
		}


		function __construct(\stdClass $data = null){
			if(!is_null($data)){
				self::inflate($this, $data);
			}
		}

	}
}


