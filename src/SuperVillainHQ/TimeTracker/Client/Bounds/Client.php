<?php

namespace SuperVillainHQ\TimeTracker\Client\Bounds {

	use Phalcon\Di;
	use SuperVillainHQ\Core\Boundary\Boundary;
	use SuperVillainHQ\TimeTracker\Client\Control\ClientService;

	/**
	 * Class Client
	 * @package SuperVillainHQ\TimeTracker\Client\Bounds
	 */
	class Client extends Boundary{

		function create(){}
		function archive(int $id){}
		function update(int $id, array $parameters){}
		function updatePartial(int $id, array $parameters){}
		function get(int $id){
			// TODO: validate and authenticate the request
			$clients = ClientService::search(['id' => $id]);
			$response = Di::getDefault()->get('response');
			$response->setContentType('application/json');
			$response->setContent(json_encode($clients));
			return $response;
		}

		function search(array $filter = []){
			// TODO: validate and authenticate the request
			$clients = ClientService::search($filter);
			$response = Di::getDefault()->get('response');
			$response->setContentType('application/json');
			$response->setContent(json_encode($clients));
			return $response;
		}
	}
}


