<?php

namespace SuperVillainHQ\TimeTracker\Api\V01M01 {

	use SuperVillainHQ\TimeTracker\Progress\Model\Progress;
	use SuperVillainHQ\TimeTracker\Task\Control\TaskService;
	use SuperVillainHQ\TimeTracker\Task\Model\Task;

	/**
	 * TaskHelper knows how to unpack a request-payload with a specific data-format and stuff it into a Task
	 * @package SuperVillainHQ\TimeTracker\Api\V01M01
	 */
	class TaskHelper{
		/**
		 * @var Task
		 */
		private $task;

		function __construct(Task $task){
			$this->task = $task;
		}

		public function appendProgressFromPayload($task, \stdClass $json){
			$taskService = new TaskService($task);
			$progress = new Progress();
			// TODO: this does not implement the details right
			$start = new \DateTime($json->start);
			$end = new \DateTime($json->end);
			$progress->setStartDate($start);
			$progress->setEndDate($end);
			$taskService->addProgress($progress);
		}
	}
}


