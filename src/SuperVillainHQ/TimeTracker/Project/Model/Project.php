<?php

namespace SuperVillainHQ\TimeTracker\Project\Model {

	use SuperVillainHQ\TimeTracker\Task\Bounds\Task;

	/**
	 * Class Project
	 * @package SuperVillainHQ\TimeTracker\Project\Model
	 */
	class Project{
		// db props
		private $id;
		private $name;
		// db relations
		private $tasks;
		// db pivot (m-n) props
		private $invoices;


		function addTask(Task $task){}

		function removeTask(Task $task){}

		function getTask(string $key):Task{
			throw new \Exception("Unable to find Task");
		}

		function findTask(string $key, $value):Task{
			throw new \Exception("Unable to find Task");
		}
	}
}


