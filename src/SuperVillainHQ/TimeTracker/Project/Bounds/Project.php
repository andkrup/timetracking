<?php

namespace SuperVillainHQ\TimeTracker\Project\Bounds {

	use SuperVillainHQ\Core\Boundary\Boundary;

	/**
	 * Class Project
	 * @package SuperVillainHQ\TimeTracker\Project\Bounds
	 */
	class Project extends Boundary{
		function create(){}
		function get(){}
		function update(){}
		function updatePartial(){}
		function archive(){}
		function index(){}
	}
}


