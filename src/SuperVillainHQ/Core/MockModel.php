<?php

namespace SuperVillainHQ\Core {

	/**
	 * Class MockModel
	 * @package SuperVillainHQ\Core
	 */
	class MockModel{
		protected static function inflate(&$model, \stdClass $data){
			// TODO: unsafe injection! we could declare a property whitelist, or reflect on public props.
			foreach ($data as $key => $value) {
				$model->{$key} = $value;
			}
		}
	}
}


