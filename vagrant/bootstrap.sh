#!/bin/bash
#

# INSTALL SECTION - should only be run at first provision/is not yet ready for multiple provision attempts
# avoid upgrading specific packages
apt-mark hold grub-common grub-pc grub-pc-bin grub2-common
apt-mark hold kbd keyboard-configuration
apt-mark hold sudo

apt-get update
apt-get upgrade -y

locale-gen "en_US.UTF-8"

echo 'PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games"' > /etc/environment
echo 'LC_ALL="en_US.UTF-8"' >> /etc/environment
echo 'LANG="en_US.UTF-8"' >> /etc/environment
echo 'LANGUAGE="en_US.UTF-8"' >> /etc/environment
echo 'LC_CTYPE="en_US.UTF-8"' >> /etc/environment
#update-alternatives --config editor
echo 'export VISUAL="/usr/bin/vim.basic"' >> /home/vagrant/.bashrc
echo 'export EDITOR="$VISUAL"' >> /home/vagrant/.bashrc

apt-get install -y curl vim wget git ntp software-properties-common python-software-properties
apt-get install -y apache2 libapache2-mod-fastcgi

# phalcon repo
curl -s https://packagecloud.io/install/repositories/phalcon/stable/script.deb.sh | sudo bash
apt-add-repository ppa:phalcon/stable -y
#apt-add-repository ppa:chris-lea/redis-server -y
# mongo repos
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list

apt-get update

apt-get install -y php7.0 php7.0-dev php7.0-zip php7.0-fpm php7.0-cli php7.0-curl php7.0-gd php7.0-intl php7.0-mbstring php7.0-xml
apt-get install -y php-msgpack php-gettext php-redis php-xdebug
apt-get install -y php-mongodb mongodb-org
apt-get install -y gettext nodejs npm zip

## mysql
#apt-get install -y php7.0-mysql
##set mysql root password for scripted mysql install
#sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password vagrant'
#sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password vagrant'
#apt-get install -y mysql-server

# postgres
apt-get install -y postgresql libpq5 postgresql-9.5 postgresql-client-9.5 postgresql-client-common postgresql-contrib
apt-get install -y php-pgsql php7.0-pgsql


# composer
curl -sS http://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer
chown root:root /usr/local/bin/composer
chmod +x /usr/local/bin/composer

# phpunit
wget https://phar.phpunit.de/phpunit.phar -O /usr/local/bin/phpunit
chown root:root /usr/local/bin/phpunit
chmod +x /usr/local/bin/phpunit

# phalcon
apt-get install -y php7.0-phalcon

# manual enable phalcon
#ln -fs /etc/php/7.0/mods-available/phalcon.ini /etc/php/7.0/fpm/conf.d/20-phalcon.ini
#ln -fs /etc/php/7.0/mods-available/phalcon.ini /etc/php/7.0/cli/conf.d/20-phalcon.ini

# Remove default apache2 log directory
#rm -r /var/log/apache2

# fix missing node symlink
ln -s /usr/bin/nodejs /usr/local/bin/node

# fix npm permissions (https://docs.npmjs.com/getting-started/fixing-npm-permissions)
#mkdir /home/vagrant/.npm-global
#npm config set prefix '/home/vagrant/.npm-global'
#export PATH=~/.npm-global/bin:$PATH
#source ~/.profile

# CONFIG SECTION - should be run at every provision

systemctl start ntp.service

a2enmod proxy_fcgi setenvif actions rewrite
a2enconf php7.0-fpm

# install/update node dependencies
npm install -g stylus aglio catchmail junit-viewer webpack

# get latest from https://github.com/mailhog/MailHog/releases/latest
# Latest: https://github.com/mailhog/MailHog/blob/master/docs/RELEASES.md
wget https://github.com/mailhog/MailHog/releases/download/v0.2.1/MailHog_linux_amd64 -O /usr/local/bin/mailhog
chmod +x /usr/local/bin/mailhog


# create upstart service (upstart seemingly won't accept a symlink in /etc/init/ to the vagrant mailhog.conf)
cp /vagrant/mailhog.service /etc/systemd/system/mailhog.service

# Start on reboot
systemctl enable mailhog

# Start background service now
systemctl start mailhog


# Remove unnessecary default vhost
unlink /etc/apache2/sites-enabled/000-default.conf


# Add apache-virtualhost config
ln -fs /vagrant/timetracking.vvb.conf /etc/apache2/sites-enabled/timetracking.vvb.conf

# Replace php.ini with out own. This also configures xdebug
unlink /etc/php/7.0/fpm/php.ini
ln -fs /vagrant/php.ini /etc/php/7.0/fpm/php.ini
# same with the cli ini
unlink /etc/php/7.0/cli/php.ini
ln -fs /vagrant/php-cli.ini /etc/php/7.0/cli/php.ini

# mongod upstart script
ln -fs /vagrant/mongodb.service /etc/systemd/system/mongodb.service
systemctl daemon-reload

# create required folders
#mkdir -p /var/backups/timetracking
#chown www-data -R /var/backups/timetracking


# ensure bin/ is in our path, if we need it
#grep -q -F 'PATH="/var/www/timetracking/bin:$PATH"' /home/vagrant/.profile || echo 'PATH="/var/www/timetracking/bin:$PATH"' >> /home/vagrant/.profile

service php7.0-fpm restart
service apache2 restart

# allow specific packages to be upgraded (upgrade manually at a later login)
apt-mark unhold grub-common grub-pc grub-pc-bin grub2-common
apt-mark unhold kbd keyboard-configuration
apt-mark unhold sudo